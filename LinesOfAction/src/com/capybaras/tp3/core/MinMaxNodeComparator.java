package com.capybaras.tp3.core;

import java.util.Comparator;

public class MinMaxNodeComparator implements Comparator<MinMaxNode>  {
	@Override
	public int compare(MinMaxNode object1, MinMaxNode object2) {
		int value1 = ((MinMaxNode)object1).getValue();
		int value2 = ((MinMaxNode)object2).getValue();
		return value2 - value1;
	}
}
