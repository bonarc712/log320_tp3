package com.capybaras.tp3.core;

import com.capybaras.tp3.model.Board;

public class MessageDecoder {
	
	//Return : true for white, false for black.
	public boolean decodeFirstTurnMessage(String message, Board board) {
		
		boolean isWhite = false;
		
		if (message.charAt(0) == '1')
			isWhite = true;
		
		String messageToDecode = message.substring(2);
		board.updateBoardFromString(messageToDecode);
		
		return isWhite;
	}
	
	//Return : last turn was good = true, last turn was bad = false
	public boolean decodeNormalTurnMessage(String message, Board board, boolean moveIsWhite) {		
		if (message.charAt(0) == '4')
			return false;
		
		String messageToDecode = message.substring(2);
		board.updateBoardFromShortString(messageToDecode, moveIsWhite);
		
		return true;
	}

}
