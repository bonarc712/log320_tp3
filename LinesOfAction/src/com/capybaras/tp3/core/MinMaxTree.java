package com.capybaras.tp3.core;

import java.util.ArrayList;

import com.capybaras.tp3.model.Board;
import com.capybaras.tp3.model.Move;

public class MinMaxTree {
	
	MinMaxNode root;
	
	public static void main(String[] args) {
		MinMaxNode node1 = new MinMaxNode();
		node1.setValue(9);
		MinMaxNode node2 = new MinMaxNode();
		node2.setValue(6);
		MinMaxNode node3 = new MinMaxNode();
		node3.setValue(11);
		MinMaxNode node4 = new MinMaxNode();
		node4.setValue(13);
		MinMaxNode node5 = new MinMaxNode();
		node5.setValue(7);
		MinMaxNode node6 = new MinMaxNode();
		node6.setValue(3);
		MinMaxNode parent1 = new MinMaxNode();
		parent1.addChild(node1);
		parent1.addChild(node2);
		MinMaxNode parent2 = new MinMaxNode();
		parent2.addChild(node3);
		parent2.addChild(node4);
		MinMaxNode parent3 = new MinMaxNode();
		parent3.addChild(node5);
		parent3.addChild(node6);
		MinMaxNode root = new MinMaxNode();
		root.addChild(parent1);
		root.addChild(parent2);
		root.addChild(parent3);
		//System.out.println("Le minimum de la racine est : " + root.minimize());
		
		//
		// Avant modifications
		// Min 7
		// Max 11
		//
		
		Board board = new Board();
		
		// Suppose/ nous donner White = 4 et Black = 3, donc si on est White, 3-4 = -1 pour l'algo de quad!
		board.updateBoardFromString("0 0 0 2 0 0 2 0 0 0 0 2 0 0 0 0 4 0 4 4 0 4 0 0 4 0 0 0 0 0 0 0 0 0 4 4 4 2 0 0 4 0 0 2 4 4 0 0 0 0 0 0 0 4 0 0 0 0 0 0 2 2 0 0");
		//board.updateBoardFromString("0 0 4 0 0 0 0 0 0 0 2 0 0 0 0 0 4 0 4 2 2 4 0 0 2 0 0 2 4 2 0 0 0 0 4 2 2 0 0 0 2 0 0 4 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
		//board.updateBoardFromString("0 0 4 4 4 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 2 0 0 2 2 4 4 0 0 0 0 2 2 2 4 0 0 0 0 0 0 2 0 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 4 0");
		board.updateBoardFromString("0 0 0 0 0 0 0 0 4 0 0 0 0 0 0 4 4 0 0 0 0 0 0 4 4 0 0 0 0 0 0 4 4 0 0 0 0 0 0 4 4 0 0 0 0 0 0 4 4 0 0 0 0 0 0 4 0 2 2 0 2 2 2 0");
		board.checkGameOver();
		if(board.gameOver()) {
		System.out.println("WIN");
		System.out.println("WINNER = " + board.getWinner());
		}
		else {
		System.out.println("FAIL");
		}
		
		//board.updateBoardFromString("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 0 0 4 0 0 0 0 0 4 0 4 4 0 0 4 0 4 0 0 0 0 0 0 0 4 0 0 0 0 0 0 4 0 0 4 4 0 0 0 0 0 0 0 0 0 0 0 0");
		//int score = board.calculateCurrentScore(true);
		
		//board.updateBoardFromString("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 2 0 0 0 0 0 0 2 0 0 2 2 0 0 0 0 0 0 2 2 2 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
		//int score2 = board.calculateCurrentScore(false);
		
		//board.updateBoardFromString("0 2 2 2 0 0 0 0 0 0 4 0 0 4 2 0 0 0 4 4 0 0 0 0 0 0 0 4 2 0 2 4 0 0 0 0 0 0 2 4 0 4 0 2 2 2 2 4 4 4 0 0 0 0 0 0 0 0 2 0 0 0 0 0");
		//int nbCells = board.getNbCellsOnDiagonal(true, 3, 7);
		//int nbCells2 = board.getNbCellsOnDiagonal(true,  0, 4);
		
		
		System.out.println("Le min est: "+ root.minimize(-100000, 100000));
		System.out.println("Le max est: "+ root.miniMax());
	}

}
