package com.capybaras.tp3.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

import com.capybaras.tp3.network.ClientSocket;

public class GameController {
	
	private BufferedInputStream input;
	private BufferedOutputStream output;
	private boolean firstMessage = true;
	
	public String getStringFromServer(ClientSocket socket) throws IOException {
		String toReturn = "";
		input = new BufferedInputStream(socket.getInputStream());
		if (firstMessage) {
			toReturn = socket.readLongMessage(input);
			if (toReturn.length() > 0)
				firstMessage = false;
		}
		else
			toReturn = socket.readShortMessage(input);
		return toReturn;
	}
	
	public void sendStringToServer(ClientSocket socket, String toSend) throws IOException {
		output = new BufferedOutputStream(socket.getOutputStream());
		socket.write(output, toSend);
		System.out.println("String sent");
	}

}
