package com.capybaras.tp3.core;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.capybaras.tp3.model.Board;
import com.capybaras.tp3.model.Move;
import com.capybaras.tp3.network.ClientSocket;

public class MenuThread {
	
	private GameController gameController;
	private MessageDecoder messageDecoder;
	private Board board;
	private boolean gameOngoing;
	private boolean firstTurn;
	private boolean hasTurn;
	private boolean playerIsWhite;
	private ClientSocket socket;
	private Move lastMove;
	private int curRank = 1;
	private boolean lastMoveValid;
	private List<Move> moveList;
	
	public MenuThread() {
		gameController = new GameController();
		messageDecoder = new MessageDecoder();
		board = new Board();
		gameOngoing = true;
		firstTurn = true;
		hasTurn = false;
		playerIsWhite = true;
		lastMove = null;
		lastMoveValid = true;
		
		moveList = new ArrayList<Move>();
	}
	
	public void executeThread() {
		try {
			socket = new ClientSocket("localhost",8888);
			while (gameOngoing) {
				startTurn();

				if (hasTurn) {
					executeTurn();
					System.out.println("Has turn");
					hasTurn = false;
				}
				
			}
			socket.close();
		}
		catch (IOException e) {
			System.out.println("Probleme de connexion");
			gameOngoing = false;
		}
	}
	
	private void startTurn() throws IOException {
		String currentTurnString = gameController.getStringFromServer(socket);
		if (currentTurnString.length() > 0) {
			hasTurn = true;
			if (firstTurn) {
				playerIsWhite = messageDecoder.decodeFirstTurnMessage(currentTurnString, board);
				System.out.println("Look at my board : ");
				System.out.println(board);
				if (!playerIsWhite)
					hasTurn = false;
			}
			else {
				boolean moveIsValid = messageDecoder.decodeNormalTurnMessage(currentTurnString, board, !playerIsWhite);
				if (!moveIsValid) {
					System.out.println("ALERT!!!!!!!!!!! LAST MOVE IS INVALID!!!!");
					System.out.println("Last move: " + lastMove.getSourceRow() + "," + lastMove.getSourceCol() + " to " + lastMove.getDestRow() + "," + lastMove.getDestCol());
					lastMove.invertMove();
					System.out.println("Last move inverted: " + lastMove.getSourceRow() + "," + lastMove.getSourceCol() + " to " + lastMove.getDestRow() + "," + lastMove.getDestCol());
					board.revertMove(lastMove);
					//board.forceMakeMove(lastMove);
					lastMoveValid = false;
					curRank++;
				}
				else {
					lastMoveValid = true;
					curRank = 1;
				}
				System.out.println("Look at my board : ");
				System.out.println(board);
			}
			firstTurn = false;
			System.out.println("Look at my string! " + currentTurnString);
		}
	}
	
	private void executeTurn() throws IOException {
		Move move = null;
		//List<Move> possibleMoves;
		if (playerIsWhite) {
			MinMaxNode aNode = new MinMaxNode();
			aNode.createTree(board, true, 3);
			int score = aNode.miniMax(curRank);
			
			move = aNode.getMove();
			/*int pos = (int)(Math.random()*possibleMoves.size()-1);
			move = possibleMoves.get(pos).ToString();
			board.makeMove(possibleMoves.get(pos));
			lastMove = possibleMoves.get(pos);
			
			for(int i = 0; i < possibleMoves.size(); ++i) {
				System.out.println("Move " + i + ": " + possibleMoves.get(i).ToString());
			}*/
			//move = "A7-C7";
		}
		else {
			MinMaxNode aNode = new MinMaxNode();
			aNode.createTree(board, false, 3);
			int score = aNode.miniMax(curRank);
			
			move = aNode.getMove();
			/*int pos = (int)(Math.random()*possibleMoves.size()-1);
			move = possibleMoves.get(pos).ToString();
			board.makeMove(possibleMoves.get(pos));
			lastMove = possibleMoves.get(pos);*/
			//move = "C8-C6";
		}
		
		lastMove = move;
		
		if(move != null) { 
			board.makeMove(move);
			System.out.println("String sent: "+ move.ToString());
			gameController.sendStringToServer(socket, move.ToString());
		}
		/*
		int pos = chooseNextMoveToPlay();
		move = moveList.get(pos).ToString();
		board.makeMove(moveList.get(pos));
		lastMove = moveList.get(pos);
		*/
//		System.out.println("Look at my moves mothafucka");
//		for (int i=0; i<possibleMoves.size(); i++) {
//			System.out.println(possibleMoves.get(i).ToString());
//		}
		/*System.out.println("Combien de moves? " + possibleMoves.size());
		System.out.println("Selected move : " + move);
		gameController.sendStringToServer(socket, move);
		lastMoveValid = true;*/
	}
	
	private int chooseNextMoveToPlay() {
		if (lastMoveValid) {
			moveList = board.getMoveList(playerIsWhite);
		}
		else {
			if (moveList.size() == 0)
				moveList = board.getMoveList(playerIsWhite);
		}
		List<Move> newMoveList = new ArrayList<Move>();
		for (int i=0; i<moveList.size(); i++) {
			MinMaxNode aNode = new MinMaxNode();
			aNode.createTreeFromSingleMove(board, playerIsWhite, 2, moveList.get(i)); //Changer profondeur ici
			int currentMoveValue = aNode.getValue();
			for (int j=0; j<newMoveList.size(); j++) {
				
			}
		}
		
		
		return 0;
	}

}
