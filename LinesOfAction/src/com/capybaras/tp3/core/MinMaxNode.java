package com.capybaras.tp3.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.capybaras.tp3.model.Board;
import com.capybaras.tp3.model.Move;

public class MinMaxNode {
	
	final int MIN_VALUE = -1000000;
	final int MAX_VALUE = 1000000;
	
	int value = 0;
	ArrayList<MinMaxNode> children;
	Board board;
	Move madeMove;
	
	public MinMaxNode() {
		children = new ArrayList<MinMaxNode>();
	}
	
	public MinMaxNode(Move move) {
		children = new ArrayList<MinMaxNode>();
		madeMove = move;
	}
	
	public void addChild(MinMaxNode child) {
		children.add(child);
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public Move getMove() {
		return madeMove;
	}
	
	public void setMove(Move move) {
		madeMove = move;
	}
	
	public int miniMax() {
		return miniMax(1);
	}
	
	public int miniMax(int rank) {
		int score = maximize(MIN_VALUE, MAX_VALUE);
		if(rank > 1 && rank < children.size()) {
			Collections.sort(children, new MinMaxNodeComparator());
			setMove(children.get(rank -1).getMove());
			setValue(children.get(rank -1).getValue());
			score = getValue();
			for(int i = 0; i < children.size(); ++i) {
				System.out.println("Value of current child : "+ children.get(i).getValue());
			}
			System.out.println("Value sent : " + score);
		}
		return score;
	}
	
	public int minimize(int alpha, int beta) {
		if (isLeaf())
			return value;
		for (int i=0; i<children.size(); i++) {
			MinMaxNode currentNode = children.get(i);
			int currentAlpha = currentNode.maximize(alpha, beta);
			if (currentAlpha < beta)
				beta = currentAlpha;
			
			if(beta <= alpha)
				return beta;
		}
		return beta;
	}
	
	public int maximize(int alpha, int beta) {
		if (isLeaf())
			return value;
		for (int i=0; i<children.size(); i++) {
			MinMaxNode currentNode = children.get(i);
			int currentBeta = currentNode.minimize(alpha, beta);
			if (currentBeta > alpha) {
				alpha = currentBeta;
				setMove(currentNode.getMove());
			}
			
			if(alpha >= beta) {
				return alpha;
			}
		}
		return alpha;
	}
	
	//
	// Creation de l'arbre de mouvements possibles. Trouve toutes les possibilites et cree
	// un board par copie selon la couleur du joueur et le nombre d'etages a creer.
	//
	public void createTree(Board board, boolean isWhitePlayer, int depth) {
		initializeTree();
		this.board = board;
		if(depth > 0) {
			ArrayList<Move> moves = board.getMoveList(isWhitePlayer);
			if(moves.size() == 0) {
				setValue((int)board.calculateCurrentScore(isWhitePlayer));
				System.out.println("Someone won");
			}
			else {
				for(int i = 0; i < moves.size(); ++i) {
					
					Move move = moves.get(i);
					MinMaxNode child = new MinMaxNode();
					Board childBoard = new Board(board);
					int moveScore = (int) board.calculateMoveScore(move);
					
					move.setScore(moveScore);
					childBoard.makeMove(move);
					child.setMove(move);
					child.setValue(move.getScore());
					child.createTree(childBoard, !isWhitePlayer, depth-1);
					children.add(child);
				}
			}
		}
	}
	
	//
	// Creation de l'arbre � partir d'un seul mouvement
	//
	public void createTreeFromSingleMove(Board board, boolean isWhitePlayer, int depth, Move move) {
		initializeTree();
		this.board = board;
		if(depth > 0) {

			MinMaxNode child = new MinMaxNode();
			Board childBoard = new Board(board);

			int moveScore = (int) board.calculateMoveScore(move);
			
			move.setScore(moveScore);
			childBoard.makeMove(move);
			child.setValue(move.getScore());
			child.createTree(childBoard, !isWhitePlayer, depth-1);
			children.add(child);
		}
	}
	
	private void initializeTree() {
		children.clear();
		children = new ArrayList<MinMaxNode>();
	}
	
	
	public boolean isLeaf() {
		return children.size() == 0;
	}


}
