package com.capybaras.tp3.model;

public class Cell {
	
	private int value;
	
	public Cell() {
		value = 0;
	}
	
	public Cell(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public String toString() {
		String toPrint;
		switch (value) {
		case 0: toPrint = "."; break;
		case 2: toPrint = "o"; break;
		case 4: toPrint = "x"; break;
		default: toPrint = " "; break;
		}
		return toPrint;
	}

}
