package com.capybaras.tp3.model;

public class Move {
	
	private int _score;
	private int _srcRow;
	private int _srcCol;
	private int _destRow;
	private int _destCol;
	private boolean _isWhitePlayer;
	
	public Move() {
		setScore(0);
		setSourceRow(0);
		setSourceCol(0);
		setDestRow(0);
		setDestCol(0);
		setPlayer(true);
	}
	
	public Move(int sourceRow, int sourceCol, int destRow, int destCol, boolean isWhitePlayer) {
		setScore(0);
		setSourceRow(sourceRow);
		setSourceCol(sourceCol);
		setDestRow(destRow);
		setDestCol(destCol);
		setPlayer(isWhitePlayer);
		
	}
	
	public int getScore() {
		return _score;
	}
	
	public int getSourceRow() {
		return _srcRow;
	}
	
	public int getSourceCol() {
		return _srcCol;
	}
	
	public int getDestRow() {
		return _destRow;
	}
	
	public int getDestCol() {
		return _destCol;
	}
	
	public boolean isWhitePlayer() {
		return _isWhitePlayer;
	}
	
	public void setScore(int score) {
		_score = score;
	}
	
	public void setSourceRow(int row) {
		if(isValid(row)) {
			_srcRow = row;
		}
	}
	
	public void setSourceCol(int col) {
		if(isValid(col)) {
			_srcCol = col;
		}
	}
	
	public void setDestRow(int row) {
		if(isValid(row)) {
			_destRow = row;
		}
	}
	
	public void setDestCol(int col) {
		if(isValid(col)) {
			_destCol = col;
		}
	}
	
	public void setPlayer(boolean isWhitePlayer) {
		_isWhitePlayer = isWhitePlayer;
	}
	
	private boolean isValid(int value) {
		return value >= 0 && value < 8;
	}
	
	public void invertMove() {
		int tempSrcRow = _srcRow;
		int tempSrcCol = _srcCol;
		_srcCol = _destCol;
		_srcRow = _destRow;
		_destCol = tempSrcCol;
		_destRow = tempSrcRow;
	}
	
	public String ToString() {
		return (Character.toString((char)(getSourceCol()+65)) + (8-getSourceRow()) +  
				Character.toString((char)(getDestCol()+65))+ (8-getDestRow()));
	}
	
	@Override
	public boolean equals(Object object) {
		Move otherMove = (Move)object;
		if(otherMove != null) {
			return otherMove.getSourceRow() == getSourceRow() && otherMove.getSourceCol() == getSourceCol() && otherMove.getDestRow() == getDestRow() && otherMove.getDestCol() == getDestCol();
		}
		return false;
	}
	
	public void calculateScore() {

	}
	

	
	
}





