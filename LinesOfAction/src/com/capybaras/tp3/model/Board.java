package com.capybaras.tp3.model;

import java.util.ArrayList;

public class Board {
	
	Cell[][] board;
	final int BLACK_CELL = 2;
	final int WHITE_CELL = 4;
	private boolean gameOver = false;
	private int winner = -1;
	private boolean lastMoveWasNomNom = false;
	
	public Board() {
		initBoardScore();
		board = new Cell[8][8];
		for (int i=0; i<8; i++) {
			for (int j=0; j<8; j++) {
				board[i][j] = new Cell();
			}
		}
	}
	
	//
	// Constructeur par copie
	//
	public Board(Board board) {
		this();
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				this.board[i][j] = new Cell(board.getCell(i, j).getValue());
			}
		}
	}
	
	public Cell getCell(int x, int y) {
		return board[x][y];
	}
	
	public Cell getCellFromString(String cell) {
		if (cell.length() < 2)
			return getCell(0,0);
		else {
			int column = (int)cell.charAt(0);
			int row = Integer.parseInt(cell.substring(1,2));
			return getCell(8-row,column-65);
		}
	}
	
	public void setCell(int x, int y, int value) {
		board[x][y].setValue(value);
	}
	
	public void updateBoardFromString(String newBoard) {
		String[] values = newBoard.split(" ");
		initBoardScore();
		for (int i=0; i<values.length; i++) {
			setCell(i/8, i%8, Integer.parseInt(values[i]));
		}
	}
	
	public void updateBoardFromShortString(String move, boolean moveIsWhite) {
		String[] values = move.split(" ");
		initBoardScore();
		Cell firstCell = getCellFromString(values[0]);
		Cell secondCell = getCellFromString(values[2]);
		firstCell.setValue(0);
		if (moveIsWhite)
			secondCell.setValue(WHITE_CELL);
		else
			secondCell.setValue(BLACK_CELL);
	}
	
	public boolean gameOver() {
		return gameOver;
	}
	
	public void checkGameOver() {
		Board tempBoard = new Board(this);
		// White marked = -WHITE_CELL;
		// Black marked = -BLACK_CELL;
		Cell[][] markBoard = tempBoard.board;
		int whiteNbCells = getNbCellsOnBoard(WHITE_CELL);
		int blackNbCells = getNbCellsOnBoard(BLACK_CELL);
		boolean whiteChecked = false;
		boolean blackChecked = false;
		boolean whiteIsNotConnected = false;
		boolean blackIsNotConnected = false;
		
		
			for(int i = 0; i < 8; ++i) {
				for(int j = 0; j < 8; ++j) {
					int curValue = this.board[i][j].getValue();
					if(curValue != 0) {
						int startx = i > 0 ? i-1 : i;
						int starty = j > 0 ? j-1 : j;
						int endx = i < 7 ? i+1 : i;
						int endy = j < 7 ? j+1 : j;
						int nbConnections = 0;
						
						if(markBoard[i][j].getValue() == WHITE_CELL) {
							if(whiteNbCells == 1) {
								gameOver = true;
								winner = WHITE_CELL;
								return;
							}
						    if(whiteChecked) {
						    	whiteIsNotConnected = true;
						    }
							whiteChecked = true;
						}
						else if(markBoard[i][j].getValue() == BLACK_CELL) {
						    if(blackNbCells == 1) {
								gameOver = true;
								winner = BLACK_CELL;
								return;
							}
						    if(blackChecked) {
						    	blackIsNotConnected = true;
						    }
							blackChecked = true;
						}
						
						for(int x = startx; x <= endx; ++x) {
							for(int y = starty; y <= endy; ++y) {
								if(x != i || y != j) {
									if( (curValue) == markBoard[x][y].getValue()) {
										markBoard[x][y].setValue(-curValue);
										++nbConnections;
									}
								}
							}
						}
						if(nbConnections > 0 && markBoard[i][j].getValue() > 0) {
							markBoard[i][j].setValue(-curValue);
						}
					}
				}
			}
			
			for(int i = 0; i < 8; ++i) {
				for(int j = 0; j < 8; ++j) {
					int curValue = markBoard[i][j].getValue();
					if(whiteIsNotConnected && blackIsNotConnected) {
						break;
					}
					if(curValue != 0) {
						if(curValue > 0) {
							if(curValue == WHITE_CELL) {
								whiteIsNotConnected = true;
							}
							else if(curValue == BLACK_CELL) {
								blackIsNotConnected = true;
							}
						}
					}
				}
			}
			
			if(!whiteIsNotConnected || !blackIsNotConnected) {
				gameOver = true;
				if(!whiteIsNotConnected && !blackIsNotConnected) {
					//tie
					winner = 0;
				}
				else if(!whiteIsNotConnected) {
					winner = WHITE_CELL;
				}
				else if(!blackIsNotConnected) {
					winner = BLACK_CELL;
				}
			}
		
	}
	
	
	private void initBoardScore() {
		gameOver = false;
		winner = -1;
	}
	
	public int getWinner() { 
		return winner;
	}
	
	//
	// Effetuer un tour sur le board en bougeant une piece
    //
	public boolean makeMove(Move move) {
		int cellColor = move.isWhitePlayer() == true ? WHITE_CELL : BLACK_CELL;
		boolean canMakeMove = canMoveTo(move.getSourceRow(), move.getSourceCol(), move.getDestRow(), move.getDestCol(), cellColor);
		if(canMakeMove) {
			setCell(move.getSourceRow(), move.getSourceCol(), 0);
			if (getCell(move.getDestRow(), move.getDestCol()).getValue() != 0) {
				lastMoveWasNomNom = true;
			}
			else
				lastMoveWasNomNom = false;
			setCell(move.getDestRow(), move.getDestCol(), cellColor);
			checkGameOver();
		}
		return canMakeMove;
	}
	
	public void forceMakeMove(Move move) {
		int cellColor = move.isWhitePlayer() == true ? WHITE_CELL : BLACK_CELL;
		setCell(move.getSourceRow(), move.getSourceCol(), 0);
		if (getCell(move.getDestRow(), move.getDestCol()).getValue() != 0) {
			lastMoveWasNomNom = true;
		}
		else
			lastMoveWasNomNom = false;
		setCell(move.getDestRow(), move.getDestCol(), cellColor);
		boolean canMakeMove = canMoveTo(move.getSourceRow(), move.getSourceCol(), move.getDestRow(), move.getDestCol(), cellColor);
		if(canMakeMove)
			checkGameOver();
	}
	
	public void revertMove(Move move) {
		int cellColor = move.isWhitePlayer() == true ? WHITE_CELL : BLACK_CELL;
		int enemyColor = move.isWhitePlayer() == true ? BLACK_CELL : WHITE_CELL;
		if (lastMoveWasNomNom)
			setCell(move.getSourceRow(), move.getSourceCol(), enemyColor);
		else
			setCell(move.getSourceRow(), move.getSourceCol(), 0);
		setCell(move.getDestRow(), move.getDestCol(), cellColor);
		lastMoveWasNomNom = false;
	}
	
	//
	// Retourne la liste des mouvements disponibles pour un joueur (blanc ou noir)
	// Chaque "Move" contient la position source (x,y) et la destination (x,y) ex: A1-D4
	//
	public ArrayList<Move> getMoveList(boolean whitePlayer) {
		
		ArrayList<Move> moves = new ArrayList<Move>();
		if(whitePlayer) {
			moves = getMoveList(WHITE_CELL);
		}
		else { 	
			moves = getMoveList(BLACK_CELL);
		}
		return moves;
	}
	
	private ArrayList<Move> getMoveList(int cellColor) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for(int i = 0; i < 8; ++i) {		
			for(int j = 0; j < 8; ++j) {
				if(board[i][j].getValue() == cellColor) {	
					
					boolean isWhitePlayer = cellColor == WHITE_CELL ? true : false;
					// SAME ROW
					int nbCellsOnRow = getNbCellsOnRow(i);	
					if(canMoveTo(i, j, i, j-nbCellsOnRow, cellColor)) {
						Move move = new Move(i, j, i, j-nbCellsOnRow, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					if(canMoveTo(i, j, i, j+nbCellsOnRow, cellColor)) {
						Move move = new Move(i, j, i, j+nbCellsOnRow, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					
					// SAME COL
					int nbCellsOnCol = getNbCellsOnCol(j);
					if(canMoveTo(i, j, i-nbCellsOnCol, j, cellColor)) {
						Move move = new Move(i, j, i-nbCellsOnCol, j, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					if(canMoveTo(i, j, i+nbCellsOnCol, j, cellColor)) {
						Move move = new Move(i, j, i+nbCellsOnCol, j, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					
					
					// DIAGONALS
					int nbCellsOnDiagLeft = getNbCellsOnDiagonal(true, i, j);
					int nbCellsOnDiagRight = getNbCellsOnDiagonal(false, i, j);
					
					/*System.out.println("I, J : ["+i+","+j+"]");
					
					System.out.println("Nombre de cells on diag left : " + nbCellsOnDiagLeft);
					System.out.println("Nombre de cells on diag right : " + nbCellsOnDiagRight);
					*/
					if(canMoveTo(i, j, i-nbCellsOnDiagLeft, j-nbCellsOnDiagLeft, cellColor)) {
						Move move = new Move(i, j, i-nbCellsOnDiagLeft ,j-nbCellsOnDiagLeft, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					if(canMoveTo(i, j, i+nbCellsOnDiagLeft, j+nbCellsOnDiagLeft, cellColor)) {
						Move move = new Move(i, j, i+nbCellsOnDiagLeft ,j+nbCellsOnDiagLeft, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					
					if(canMoveTo(i, j, i-nbCellsOnDiagRight, j-nbCellsOnDiagRight, cellColor)) {
					    Move move = new Move(i, j, i-nbCellsOnDiagRight ,j-nbCellsOnDiagRight, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
					if(canMoveTo(i, j, i+nbCellsOnDiagRight, j+nbCellsOnDiagRight, cellColor)) {
						Move move = new Move(i, j, i+nbCellsOnDiagRight ,j+nbCellsOnDiagRight, isWhitePlayer);
						if(!moves.contains(move)) {
							moves.add(move);
						}
					}
				}
			}
		}
		return moves;
	}
	
	public float calculateCurrentScore(boolean isWhitePlayer) {
		Move move = new Move();
		move.setPlayer(isWhitePlayer);
		return calculateMoveScore(move);
	}
	
	public float calculateMoveScore(Move move) {
		
		if(move == null) 
			return 0;
		
		float myScore = 0; // Total
		int cellColor = move.isWhitePlayer() ? WHITE_CELL : BLACK_CELL;
		boolean moveSuccess = false;
		
		Cell[][] newBoard = null;
		Board simBoard = new Board(this);
		
		// TODO AUTREMENT ICI
		if(move.getSourceRow() != move.getDestRow() || move.getSourceCol() != move.getDestCol()) {
			moveSuccess = simBoard.makeMove(move);
			newBoard = simBoard.board;
		}
		
		if(simBoard.gameOver()) {
			myScore = 9001;
		}
		else if(!moveSuccess) {
			// Ca ne devrait pas arriver. Si le move est invalide, que fait-on?
			myScore = 0; 
		}
		else {
			for(int i = -1; i <= 7; i++) {
				for(int j = -1; j <= 7; j++) {
					
					int topLeftCell = 0;
					int topRightCell = 0;
					int bottomLeftCell = 0;
					int bottomRightCell = 0;
					
					int centralScore = 0;
					
					if(i >= 0 || j >= 0) {
						if(i >= 0 && j >= 0) {
							topLeftCell = newBoard[i][j].getValue();
							centralScore = getCentralHeuristicScore(i, j, topLeftCell, cellColor);
							if(i < 7 || j < 7) {
								if(i < 7 && j < 7) {
									topRightCell = newBoard[i][j+1].getValue();
								    bottomLeftCell = newBoard[i+1][j].getValue();
								    bottomRightCell = newBoard[i+1][j+1].getValue();
								}
								else if(i < 7) {
									bottomLeftCell = newBoard[i+1][j].getValue();
								}
								else if(j < 7) {
									topRightCell = newBoard[i][j+1].getValue();
								}
							}
						}
						else if(i >= 0) {
							topRightCell = newBoard[i][j+1].getValue();
							if(i < 7) {
								bottomRightCell = newBoard[i+1][j+1].getValue();
							}
						}
						else if(j >= 0) {
							bottomLeftCell = newBoard[i+1][j].getValue();
							if(j < 7) {
								bottomRightCell = newBoard[i+1][j+1].getValue();
							}
						}
						else {
							bottomRightCell = newBoard[i+1][j+1].getValue();
						}
					}
					
					myScore += centralScore;
					myScore += getQuadHeuristicScore(topLeftCell, topRightCell, bottomLeftCell, bottomRightCell, cellColor);
					myScore += getLinkBreakHeuristicScore(move);
					
				}
			}
		}
		
		return myScore;
	}
	
	private float getQuadHeuristicScore(int topLeftCell, int topRightCell, int bottomLeftCell, int bottomRightCell, int cellColor) {

		int curCellCount = 0;
		int otherCellCount = 0;
		
		if(topLeftCell == cellColor) ++curCellCount;
		else if(topLeftCell != 0) ++otherCellCount;
		if(topRightCell == cellColor) ++curCellCount;
		else if(topRightCell != 0) ++otherCellCount;
		if(bottomLeftCell == cellColor) ++curCellCount;
		else if(bottomLeftCell != 0) ++otherCellCount;
		if(bottomRightCell == cellColor) ++curCellCount;
		else if(bottomRightCell != 0) ++otherCellCount;
		
		float myScore = getSubQuadHeuristicScore(topLeftCell, topRightCell, bottomLeftCell, bottomRightCell, cellColor, curCellCount);
		float otherScore = getSubQuadHeuristicScore(topLeftCell, topRightCell, bottomLeftCell, bottomRightCell, cellColor, otherCellCount);
		
		return otherScore - myScore;
	}
	
	private float getSubQuadHeuristicScore(int topLeftCell, int topRightCell, int bottomLeftCell, int bottomRightCell, int cellColor, int cellCount) {
		float score = 0;
		
		switch(cellCount) {
		case 1: score = 0.25f;//= 2;
			break;
		case 2:
				if(topLeftCell == cellColor) {
					if(bottomRightCell == cellColor)
						score = -0.5f;//-4;
				}
				else if(topRightCell == cellColor) {
					if(bottomLeftCell == cellColor)
						score = -0.5f;//-4;
				}
			break;
		case 3: score = -0.25f;//-2;
			break;
		}
		
		return score;
	}
	
	private int getCentralHeuristicScore(int row, int col, int curCellColor, int playerColor) {
		
		int score = 0;
		int lowerBounds = 1;
		int higherBounds = 6;
		for(int i = 1; i < 4; ++i) {
			if(row >= lowerBounds && row <= higherBounds && col >= lowerBounds && col <= higherBounds) {
				++score;
				++lowerBounds;
				--higherBounds;
			}
			else {
				i = 4;
			}
		}
		
		int myScore = 0;
		int otherScore = 0;
		if(curCellColor == playerColor) {
			myScore = score;
		}
		else if(curCellColor != 0) {
			otherScore = score;
		}
		
		return myScore - otherScore;
	}
	
	//
	// Indique si, d'apres le Board courant, l'utilisateur de couleur "cellColor" peut se deplacer a la 
	// position desiree
	//
	public boolean canMoveTo(int sourceRow, int sourceCol, int destRow, int destCol, int cellColor) {
		
		if(sourceRow < 0 || sourceRow > 7 ||
		   sourceCol < 0 || sourceCol > 7 ||
	   	   destRow < 0 || destRow > 7 ||
	   	   destCol < 0 || destCol > 7 ||
	   	   (cellColor != BLACK_CELL && cellColor != WHITE_CELL)) return false;
		
		int otherCellColor = cellColor == WHITE_CELL ? BLACK_CELL : WHITE_CELL;
		
		int startCol = sourceCol;
		int endCol = destCol;
		if(sourceCol > destCol) {
			startCol = destCol;
			endCol = sourceCol;
		}
		
		int startRow = sourceRow;
		int endRow = destRow;
		if(sourceRow > destRow) {
			startRow = destRow;
			endRow = sourceRow;
		}
		
		
		int curValue = 0;
		if(sourceRow == destRow) {
			while(startCol <= endCol) {
				curValue = board[sourceRow][startCol].getValue();
				if(curValue != 0 && (curValue == otherCellColor && startCol != endCol)) {
					return false;
				}
				++startCol;
			}
		}
		else if(sourceCol == destCol) {
			while(startRow <= endRow) {
				curValue = board[startRow][sourceCol].getValue();
				if(curValue != 0 && (curValue == otherCellColor && startRow != endRow)) {
					return false;
				}
				++startRow;
			}
		}
		else {
			while(startRow <= endRow) {
				curValue = board[startRow][startCol].getValue();
				if(curValue != 0 && (curValue == otherCellColor && startRow != endRow)) {
					return false;
				}
				++startRow;
				++startCol;
			}
		}
		
		int destinationValue = board[destRow][destCol].getValue();
		if(destinationValue == cellColor) {
			//Move move = new Move(sourceRow, sourceCol, destRow, destCol, cellColor == WHITE_CELL);
			//System.out.println("ESSAI DE MANGER SON AMI DE: curValue = "+curValue+" move: "+move.ToString());
			return false;
		}
		
		return true;
	}
	
	//
	// NB CELLS ON COL (BLACK_CELL OR WHITE_CELL)
	//
	public int getNbCellsOnCol(int col) {
		int count = 0;
		if(col >= 0 && col < 8) {
			for(int i = 0; i < 8; ++i) {
				if(board[i][col].getValue() != 0) {
					++count;
				}
			}
		}
		return count;
	}
	
	//
	// NB CELLS ON ROW (BLACK_CELL OR WHITE_CELL)
	//
	public int getNbCellsOnRow(int row) {
		int count = 0;
		if(row >= 0 && row < 8) {
			for(int i = 0; i < 8; ++i) {
				if(board[row][i].getValue() != 0) {
					++count;
				}
			}
		}
		return count;
	}
	
	// 
	// NB CELLS ON DIAG (BLACK_CELL OR WHITE_CELL)
	//
	// si "topLeftToBottomRight" = true, parcourt comme ceci: "\"
	// sinon on parcourt comme ceci: "/"
	//
	public int getNbCellsOnDiagonal(boolean topLeftToBottomRight, int row, int col) {
		int count = 0;

		
		if (topLeftToBottomRight) {
			int diff = col-row;
			if (diff > 0) {
				for (int i=0; i<8-diff; i++) {
					if (board[i][i+diff].getValue() != 0)
						count++;
				}
			}
			else {
				diff = Math.abs(diff);
				for (int i=0; i<8-diff; i++) {
					if (board[i+diff][i].getValue() != 0)
						count++;
				}
			}
		}
		else {
			int sum = col+row;
			if (sum > 7) {
				for (int i=7; i>sum-8; i--) {
					if (board[sum-i][i].getValue() != 0)
						count++;
				}
			}
			else {
				for (int i=0; i<=sum; i++) {
					if (board[i][sum-i].getValue() != 0)
						count++;
				}
			}
		}
		
		return count;
	}
	
	public int getNbCellsOnBoard(int playerColor) {
		int count = 0;
		for(int i = 0; i < 8; ++i) {
			for(int j = 0; j < 8; ++j) { 
				if(board[i][j].getValue() == playerColor) {
					++count;
				}
			}
		}
		return count;
	}
	
	public String toString() {
		String toReturn = "";
		for (int i=0; i<8; i++) {
			for (int j=0; j<8; j++) {
				toReturn = toReturn + " " + getCell(i,j).toString();
			}
			toReturn = toReturn + "\n";
		}
		return toReturn;
	}
	
	public void extraCalculation(Move move){
		checkLinkMake(move);
		LocateMainCluster();
		checkLinkBreak();
		
	}

	//check le plus gros cluster de cell et boost les moves a ce spot
	public void LocateMainCluster() {

	}
	

	
	//Check pour la creation d'un lien entre differente cellule
		public int checkLinkMake(Move move) {
			int additionalScore = 0;
			int clusterSize = 0;
		 
			 clusterSize = checkSurrounding(move.getDestRow(),move.getDestCol(), move.isWhitePlayer());
		     while(clusterSize >= 1)//join a cluster
		     {  	 
		    	 --clusterSize;
		    	 additionalScore += 1;
		     } 
		   
		     clusterSize = checkSurrounding(move.getSourceRow(),move.getSourceCol(),move.isWhitePlayer());
		     while(clusterSize >= 1)//join a cluster
		     {  	 
		    	 --clusterSize;
		    	 additionalScore -= 1;
		     }
		     
		     return additionalScore;
		}
	    	 
	     
	     public int checkSurrounding(int row, int col, boolean isWhite){
	    	int size = 0;
	    	int comparator = 0;
	    	 
	    	 if(isWhite){
	    		 comparator = 4;
	    	 }
	    	 else{
	    		 comparator = 2;
	    	 }
	    	 if (row < 7) {
		    	 if(board[row+1][col] != null)
			    	 if(board[row+1][col].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (row < 7 && col < 7) {
		    	 if(board[row+1][col+1] != null)
			    	 if(board[row+1][col+1].getValue() == comparator)
			    		 ++size;
		     }
	    	 if (col < 7) {
		    	 if(board[row][col+1] != null)	    	 
			    	 if(board[row][col+1].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (row > 0) {
		    	 if(board[row-1][col] != null)	    	 
			    	 if(board[row-1][col].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (row > 0 && col > 0) {
		    	 if(board[row-1][col-1] != null)	    	 
			    	 if(board[row-1][col-1].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (col > 0) {
		    	 if(board[row][col-1] != null)	    	 
			    	 if(board[row][col-1].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (row < 7 && col > 0) {
		    	 if(board[row+1][col-1] != null)	    	 
			    	 if(board[row+1][col-1].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 if (row > 0 && col < 7) {
		    	 if(board[row-1][col+1] != null)	    	 
			    	 if(board[row-1][col+1].getValue() == comparator)
			    		 ++size;
	    	 }
	    	 
	 
	    	 return size;
	     }
	     
	 	//Check pour le bris d'un cluster/ligne
	 	public void checkLinkBreak() {
	 		
	 	}
	 	
	 	public int getLinkBreakHeuristicScore(Move move) {
	 		int clusterSize = checkSurrounding(move.getSourceRow(), move.getSourceCol(), move.isWhitePlayer());
	 		//System.out.println("Cluster size : " + clusterSize);
	 		if (clusterSize == 1)
	 			return 3;
	 		if (clusterSize == 2)
	 			return 2;
	 		return 1;
	 	}
	
}
