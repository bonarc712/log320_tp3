package com.capybaras.tp3.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientSocket extends Socket {
	
	public ClientSocket() throws IOException {
		super("localhost", 8888);
	}

	public ClientSocket(String host, int port) throws IOException {
		super(host,port);
	}
	
	public String readShortMessage(BufferedInputStream input) throws IOException {
		return readMessage(input, 16);
	}
	
	public String readLongMessage(BufferedInputStream input) throws IOException {
		return readMessage(input, 1024);
	}
	
	private String readMessage(BufferedInputStream input, int maxSize) throws IOException {
		String toReturn = "";
		byte[] bytes = new byte[maxSize];
		int size = input.available();
		int hasWorked = input.read(bytes, 0, size);
        String messageReceived = new String(bytes).trim();
        if (messageReceived.length() > 0)
		System.out.println("Message: " + messageReceived);
		if (hasWorked != -1) {
			toReturn = messageReceived;
		}
		return toReturn;
		
	}
	
	public void write(BufferedOutputStream output, String text) throws IOException {
		output.write(text.getBytes(), 0, text.length());
		output.flush();
	}
	
}
